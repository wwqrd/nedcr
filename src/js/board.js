var _ = require('underscore');
var Rock = require('./rock');
var Diamond = require('./diamond');

var Board = function() {
  this._items = [];
  this._points = 0;
};

var proto = Board.prototype;

proto.addItem = function(item, position) {
  var itemIndex;

  this._items.push({
    item: item,
    position: position
  });

  itemIndex = this._items.length - 1;

  item.on('move', _.bind(function(movement) {
    this.onMove(itemIndex, movement);
  }, this));
};

proto.onMove = function(itemIndex, movement) {
  var item = this._items[itemIndex];

  var newPosition = {
    x: item.position.x + movement.x,
    y: item.position.y + movement.y,
  }

  if(this.itemAtPosition(newPosition) instanceof Diamond) {
    this.addPoint();
    this.removeItem(this.itemAtPosition(newPosition));
  }

  if(!this.isItemAtPosition(newPosition)) {
    this._items[itemIndex].position = newPosition;
  }
};

proto.isItemAtPosition = function(position) {
  return !!this.itemAtPosition(position);
};

proto.itemAtPosition = function(position) {
  var itemPosition;

  for(itemIndex in this._items) {
    itemPosition = this._items[itemIndex].position;
    if(itemPosition.x === position.x && itemPosition.y === position.y) {
      return this._items[itemIndex].item;
    }
  }
};

proto.removeItem = function(item) {
  for(itemIndex in this._items) {
    if(this._items[itemIndex].item === item) {
      this._items.splice(itemIndex, 1);
      return;
    }
  }
};

proto.item = function(item) {
  for(itemIndex in this._items) {
    if(this._items[itemIndex].item === item) {
      return this._items[itemIndex];
    }
  }
};

proto.addPoint = function() {
  this._points += 1;
};

proto.points = function() {
  return this._points;
};

module.exports = Board;
