var _ = require('underscore');
var Backbone = require('backbone');

var Robot = function(options) {
  options = options || {};

  this._orientation = options.orientation ? options.orientation : 0;
};

_.extend(Robot.prototype, Backbone.Events);

var proto = Robot.prototype;

var setOrientation = function(change) {
  if(change) {
    this._orientation = (360 + this._orientation + change) % 360;
  }
};

proto.orientation = function() {
  return this._orientation;
};

proto.turnLeft = function() {
  setOrientation.call(this, -90);
};

proto.turnRight = function() {
  setOrientation.call(this, 90);
};

proto.move = function() {
  var x = Math.round(Math.sin(Math.PI * this.orientation()/180));
  var y = Math.round(Math.cos(Math.PI * this.orientation()/180));

  this.trigger('move', {x: x, y: y});
};

module.exports = Robot;
