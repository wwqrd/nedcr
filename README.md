# Nedcr

Never-ending diamond collecting robot.

# Requirements

- node.js
    - gulp (`npm install -g gulp`)

# Run the tests

    $ gulp test

# Issues

- No ability to write macros, thought about implementing them something like:

        game.prototype.addMacro = function(name, method) {
          this.robot[name] = function() {
            method(this.robot);
          };
        };

- Terminology needs some more thought: The `move` event is used to notify the board of a move, but in the game a `move` would be when a macro is run.
- Methods in board relating to item management could be refactored.

