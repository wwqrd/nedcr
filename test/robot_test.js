var test_helper = require('./test_helper');
var expect = test_helper.expect;
var sinon = test_helper.sinon;
var Robot = require(test_helper.src('robot'));

describe("Robot", function() {

  var robot = new Robot();

  describe("move", function() {
    var robot;
    var moveSpy;

    beforeEach(function() {
      robot = new Robot();
      moveSpy = sinon.spy();
      robot.on('move', moveSpy);
    });

    describe("when facing north", function() {
      it("moves north", function() {
        robot.move();
        expect(moveSpy.withArgs({x: 0, y: 1}).calledOnce).to.be.true;
      });
    });

    describe("when facing west", function() {
      it("moves west", function() {
        robot.turnLeft();
        robot.move();
        expect(moveSpy.withArgs({x: -1, y: 0}).calledOnce).to.be.true;
      });
    });

    describe("when facing east", function() {
      it("moves east", function() {
        robot.turnRight();
        robot.move();
        expect(moveSpy.withArgs({x: 1, y: 0}).calledOnce).to.be.true;
      });
    });

    describe("when facing south", function() {
      it("moves south", function() {
        robot.turnRight();
        robot.turnRight();
        robot.move();
        expect(moveSpy.withArgs({x: 0, y: -1}).calledOnce).to.be.true;
      });
    });

  });

  describe("turning", function() {
    var robot = new Robot();

    describe("turnLeft", function() {
      it("turns left", function() {
        robot.turnLeft();
        expect(robot.orientation()).to.eq(270);
      });
    });

    describe("turnRight", function() {
      it("turns right", function() {
        robot.turnRight();
        expect(robot.orientation()).to.eq(0);
      });
    });

  });

});
