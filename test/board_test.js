var test_helper = require('./test_helper');
var _ = require('underscore');
var Backbone = require('backbone');
var Board = require(test_helper.src('board'));
var Rock = require(test_helper.src('rock'));
var Diamond = require(test_helper.src('diamond'));

var expect = test_helper.expect;
var sinon = test_helper.sinon;

describe("Board", function() {

  var board;

  beforeEach(function() {
    board = new Board();
  });

  describe("item moves", function() {

    var fakeRobot;

    beforeEach(function() {
      fakeRobot = _.clone(Backbone.Events);
      board.addItem(fakeRobot, {x: 0, y: 0});
    });

    describe("there is not an obstruction", function() {
      it("move freely", function() {
        fakeRobot.trigger('move', {x: 1, y: 0});
        expect(board.item(fakeRobot).position).to.eql({x: 1, y:0});
      });
    });

    describe("there is an obstruction", function() {
      beforeEach(function() {
        board.addItem(new Rock(), {x: 1, y: 0});
        fakeRobot.trigger('move', {x: 1, y: 0});
      });

      it("does not move", function() {
        expect(board.item(fakeRobot).position).to.eql({x: 0, y:0});
      });
    });

    describe("there is a point scorer", function() {
      var position = {x: 1, y: 0};
      beforeEach(function() {
        board.addItem(new Diamond(), position);
        fakeRobot.trigger('move', position);
      });

      it("add a point", function() {
        expect(board.points()).to.eq(1);
      });

      it("removes point scorer from board", function() {
        expect(board.itemAtPosition(position)).to.not.be.an.instanceof(Diamond);
      });
    });

    describe("there is not a point scorer", function() {
      it("don't add a point", function() {
        fakeRobot.trigger('move', {x: 1, y: 0});
        expect(board.points()).to.eq(0);
      });
    });
  });

});
