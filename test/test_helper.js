var src_dir = '../src/js/';
var expect = require('chai').expect;
var sinon = require('sinon');

var src = function(filename) {
  return src_dir + filename;
};

exports.src = src;
exports.expect = expect;
exports.sinon = sinon;
